package com.deputat.lab1;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.deputat.lab1.utils.Operation;
import com.deputat.lab1.utils.Set;

public class MainActivity extends AppCompatActivity {

    private TextView mTextViewSetA;
    private TextView mTextViewSetB;
    private TextView mTextViewUnion;
    private TextView mTextViewIntersection;
    private TextView mTextViewDifference;
    private TextView mTextViewSymDifference;
    private TextView mTextViewIsIncluded;
    private TextView mTextViewAreEqual;
    private EditText mEditTextSetA;
    private EditText mEditTextSetB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mTextViewSetA = (TextView) findViewById(R.id.tv_set_a);
        mTextViewSetB = (TextView) findViewById(R.id.tv_set_b);
        mTextViewUnion = (TextView) findViewById(R.id.tv_union);
        mTextViewIntersection = (TextView) findViewById(R.id.tv_intersection);
        mTextViewDifference = (TextView) findViewById(R.id.tv_difference);
        mTextViewSymDifference = (TextView) findViewById(R.id.tv_sym_difference);
        mTextViewIsIncluded = (TextView) findViewById(R.id.tv_is_included);
        mTextViewAreEqual = (TextView) findViewById(R.id.tv_are_equal);

        mEditTextSetA = (EditText) findViewById(R.id.et_set_a);
        mEditTextSetB = (EditText) findViewById(R.id.et_set_b);

        findViewById(R.id.btn_calculate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculate();
            }
        });

        mEditTextSetA.setText("6,8,2,7,9,5");
        mEditTextSetB.setText("2,8,9,3,1,6");
    }

    private void calculate() {
        final String[] stringsA = mEditTextSetA.getText().toString().trim().split(",");
        final int[] arrayA = new int[stringsA.length];

        for (int i = 0; i < stringsA.length; i++) {
            arrayA[i] = Integer.parseInt(stringsA[i].trim());
        }

        final String[] stringsB = mEditTextSetB.getText().toString().trim().split(",");
        final int[] arrayB = new int[stringsB.length];

        for (int i = 0; i < stringsB.length; i++) {
            arrayB[i] = Integer.parseInt(stringsB[i].trim());
        }

        final Set a = new Set("A", arrayA);
        final Set b = new Set("B", arrayB);

        mTextViewSetA.setText(a.toString());
        mTextViewSetB.setText(b.toString());

        mTextViewUnion.setText(a.getName() + " " + Operation.UNION + " " + b.getName() +
                " = " + Operation.union(a, b));
        mTextViewIntersection.setText(a.getName() + " " + Operation.INTERSECTION + " " + b.getName() +
                " = " + Operation.intersection(a, b));
        mTextViewDifference.setText(a.getName() + " " + Operation.DIFFERENCE + " " + b.getName() +
                " = " + Operation.difference(a, b));
        mTextViewSymDifference.setText(a.getName() + " " + Operation.SYM_DIFFERENCE + " " + b.getName() +
                " = " + Operation.symDifference(a, b));
        mTextViewIsIncluded.setText(a.getName() + " " +
                (Operation.isIncluded(a, b) ? Operation.INCLUDED : Operation.NOT_INCLUDED) + " " + b.getName());
        mTextViewAreEqual.setText(a.getName() + " " +
                (Operation.areEqual(a, b) ? Operation.EQUAL : Operation.NOT_EQUAL) + " " + b.getName());
    }
}
