package com.deputat.lab1.utils;

public class Set {
    private int[] array;
    private String name;

    private static final int[] EMPTY_ARRAY = {};

    public Set(String name) {
        if (name == null){
            throw new NullPointerException("name cannot be null");
        }

        this.name = name;
        array = EMPTY_ARRAY;
    }

    public Set(String name, int... array) {
        if (name == null){
            throw new NullPointerException("name cannot be null");
        }

        this.name = name;
        this.array = uniqueValues(array);
    }



    public static int[] uniqueValues(final int[] array) {
        if (array == null || array.length == 0){
            return EMPTY_ARRAY;
        }

        int size = array.length;
        final int[] copy = new int[size];

        for (int i = 0; i < array.length; i++) {
            if (contains(copy, array[i])){
                size--;
            }else {
                copy[i] = array[i];
            }
        }

        final int[] uniqueValues = new int[size];
        int number = 0;

        for (int t :
                array) {
            if (!contains(uniqueValues, t)){
                uniqueValues[number] = t;
                number++;
            }
        }

        return uniqueValues;
    }

    public static boolean contains(int[] uniqueValues, int i) {
        for (int i1 :
                uniqueValues) {
            if (i1 == i) {
                return true;
            }
        }

        return false;
    }

    public boolean contains(int i){
        return contains(array, i);
    }

    public void addAll(final Set set){

        int size = array.length;

        for (int i :
                set.array) {
            if (!contains(i)) {
                size++;
            }
        }

        final int[] copy = this.array;
        this.array = new int[size];

        for (int i = 0; i < copy.length; i++) {
            array[i] = copy[i];
        }

        int number = 0;

        for (int i :
                set.array) {
            if (!contains(i)) {
                array[copy.length + number] = i;

                number++;
            }
        }
    }

    public void setAll(int... array) {
        this.array = uniqueValues(array);
    }

    public static String getAllString(int[] a) {
        int iMax = a.length - 1;
        if (iMax == -1)
            return "{}";

        final StringBuilder b = new StringBuilder();
        b.append("{");

        for (int i = 0; ; i++) {
            b.append(a[i]);

            if (i == iMax)
                return b.append("}").toString();
            b.append(", ");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[] toArray() {
        return array;
    }

    @Override public String toString() {
        return name + getAllString(array);
    }
}
