package com.deputat.lab1.utils;

public class Operation {

    public static final String UNION = "U";
    public static final String INTERSECTION  = "∩";
    public static final String DIFFERENCE  = "\\";
    public static final String SYM_DIFFERENCE  = "⊕";
    public static final String INCLUDED  = "⊂";
    public static final String NOT_INCLUDED  = "∉";
    public static final String EQUAL  = "=";
    public static final String NOT_EQUAL  = "≠";

    public static Set union(Set a, Set b){
        final Set c = new Set("C", a.toArray());

        c.addAll(b);

        return c;
    }

    public static Set intersection(Set a, Set b){
        int size = 0;

        for (int i :
                a.toArray()) {
            if (Set.contains(b.toArray(), i)) {
                size++;
            }
        }

        if (size == 0){
            return new Set("D");
        }

        final int[] array = new int[size];
        int number = 0;

        for (int i :
                a.toArray()) {
            if (Set.contains(b.toArray(), i)) {
                array[number] = i;
                number++;
            }
        }

        return new Set("D", array);
    }

    public static Set difference(Set a, Set b){
        int size = a.toArray().length;

        for (int i :
                a.toArray()) {
            if (Set.contains(b.toArray(), i)){
                size--;
            }
        }

        if (size == 0){
            return new Set("E");
        }

        final int[] array = new int[size];
        int number = 0;

        for (int i :
                a.toArray()) {
            if (!Set.contains(b.toArray(), i)){
                array[number] = i;
                number++;
            }
        }

        return new Set("E", array);
    }

    public static Set symDifference(Set a, Set b){
        final Set aDifference = difference(a, b);
        final Set bDifference = difference(b, a);

        aDifference.addAll(bDifference);

        return new Set("F", aDifference.toArray());
    }

    public static boolean isIncluded(Set a, Set b){
        boolean isIncluded = true;

        for (int i :
                a.toArray()) {
            if (!Set.contains(b.toArray(), i)){
                isIncluded = false;
            }
        }

        return isIncluded;
    }

    public static boolean areEqual(Set a, Set b){
        return isIncluded(a, b) && isIncluded(b, a);
    }
}
